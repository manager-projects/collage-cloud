package com.cloud.collage.collagecloud.security.entity;

import com.cloud.collage.collagecloud.security.enums.NameRole;

import javax.persistence.*;

@Entity @Table(name = "tbl_roles")
public class Role {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false) @Enumerated(EnumType.STRING)
    private NameRole nameRole;

    public Role() {
    }

    public Role(NameRole nameRole) {
        this.nameRole = nameRole;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NameRole getNameRole() {
        return nameRole;
    }

    public void setNameRole(NameRole nameRole) {
        this.nameRole = nameRole;
    }
}
