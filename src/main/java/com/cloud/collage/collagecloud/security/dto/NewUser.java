package com.cloud.collage.collagecloud.security.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

public class NewUser {
    @NotBlank(message = "The user is required")
    @NotEmpty(message = "The user is required")
    private String username;
    @NotBlank(message = "The email is required")
    @NotEmpty(message = "The email is required")
    @Email(message = "The e-mail format is incorrect")
    private String email;
    @NotBlank(message = "The password is required")
    @NotEmpty(message = "The password is required")
    private String password;

    private Set<String> roles = new HashSet<>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
