package com.cloud.collage.collagecloud.security.repository;

import com.cloud.collage.collagecloud.security.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String user);
    boolean existsByUsername(String user);
    boolean existsByEmail(String email);
}
