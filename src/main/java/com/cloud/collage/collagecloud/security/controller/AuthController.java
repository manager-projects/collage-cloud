package com.cloud.collage.collagecloud.security.controller;

import com.cloud.collage.collagecloud.security.dto.JwtDto;
import com.cloud.collage.collagecloud.security.dto.NewUser;
import com.cloud.collage.collagecloud.security.dto.UserLogin;
import com.cloud.collage.collagecloud.security.entity.Role;
import com.cloud.collage.collagecloud.security.entity.User;
import com.cloud.collage.collagecloud.security.enums.NameRole;
import com.cloud.collage.collagecloud.security.jwt.JwtProvider;
import com.cloud.collage.collagecloud.security.service.RoleService;
import com.cloud.collage.collagecloud.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private Map<String, Object> response;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private JwtProvider jwtProvider;

    @PostMapping("/new")
    public ResponseEntity<?> newUser(@Valid @RequestBody NewUser user){
        response = new HashMap<>();
        User user_db = new User(
                user.getUsername(), user.getEmail(), passwordEncoder.encode(user.getPassword())
        );
        Set<Role> roles = new HashSet<>();
        roles.add(roleService.getNameRole(NameRole.ROLE_USER).get());
        if (user.getRoles().contains("admin")){
            roles.add(roleService.getNameRole(NameRole.ROLE_ADMIN).get());
        }
        if (user.getRoles().contains("student")){
            roles.add(roleService.getNameRole(NameRole.ROLE_STUDENT).get());
        }
        if (user.getRoles().contains("teacher")){
            roles.add(roleService.getNameRole(NameRole.ROLE_TEACHER).get());
        }
        if (user.getRoles().contains("coordinator")){
            roles.add(roleService.getNameRole(NameRole.ROLE_COORDINATOR).get());
        }

        user_db.setRoles(roles);
        userService.saveUser(user_db);
        response.put("response", "User created");

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<JwtDto> login(@Valid @RequestBody UserLogin userLogin){
        Authentication authentication =
                authenticationManager
                        .authenticate(new UsernamePasswordAuthenticationToken(
                                userLogin.getUsername(), userLogin.getPassword()
                        ));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtProvider.genereteToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(), userDetails.getAuthorities());
        return new ResponseEntity<>(jwtDto, HttpStatus.OK);
    }

    @GetMapping("/user")
    public ResponseEntity<?> getUser(){
        response = new HashMap<>();
        User user = userService.getByUsername("david").get();
        response.put("user", user);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
