package com.cloud.collage.collagecloud.security.service;

import com.cloud.collage.collagecloud.security.entity.User;

import java.util.Optional;

public interface UserService {

    Optional<User> getByUsername(String user);
    void saveUser(User user);

}
