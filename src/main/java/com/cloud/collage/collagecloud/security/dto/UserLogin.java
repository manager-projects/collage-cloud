package com.cloud.collage.collagecloud.security.dto;

import javax.validation.constraints.NotBlank;

public class UserLogin {
    @NotBlank(message = "The user is required")
    private String username;
    @NotBlank(message = "The password is required")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
