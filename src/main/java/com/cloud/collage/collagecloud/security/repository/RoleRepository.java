package com.cloud.collage.collagecloud.security.repository;

import com.cloud.collage.collagecloud.security.entity.Role;
import com.cloud.collage.collagecloud.security.enums.NameRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByNameRole(NameRole nameRole);
}
