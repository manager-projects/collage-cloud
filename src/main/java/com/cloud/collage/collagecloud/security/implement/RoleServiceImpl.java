package com.cloud.collage.collagecloud.security.implement;

import com.cloud.collage.collagecloud.security.entity.Role;
import com.cloud.collage.collagecloud.security.enums.NameRole;
import com.cloud.collage.collagecloud.security.repository.RoleRepository;
import com.cloud.collage.collagecloud.security.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Optional<Role> getNameRole(NameRole role) {
        return roleRepository.findByNameRole(role);
    }

    @Override
    public void saveRole(Role role) {
        roleRepository.save(role);
    }
}
