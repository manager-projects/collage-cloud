package com.cloud.collage.collagecloud.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity @Table(name = "tbl_document_types")
public class DocumentTypes implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "document_id", nullable = false)
    private Long id;
    @Column(name = "document_type", nullable = false)
    private String documentType;
    @Column(nullable = false)
    private boolean status;
    @Column(name = "created_at", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    public DocumentTypes() {
    }

    public DocumentTypes(String documentType, boolean status, Date createdAt) {
        this.documentType = documentType;
        this.status = status;
        this.createdAt = createdAt;
    }

    @PrePersist
    public void prePersist(){
        this.createdAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
