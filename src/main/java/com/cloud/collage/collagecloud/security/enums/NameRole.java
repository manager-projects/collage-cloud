package com.cloud.collage.collagecloud.security.enums;

public enum NameRole {
    ROLE_ADMIN, ROLE_USER, ROLE_STUDENT, ROLE_TEACHER, ROLE_COORDINATOR
}
