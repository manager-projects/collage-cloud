package com.cloud.collage.collagecloud.security.implement;

import com.cloud.collage.collagecloud.security.entity.User;
import com.cloud.collage.collagecloud.security.repository.UserRepository;
import com.cloud.collage.collagecloud.security.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<User> getByUsername(String user) {
        return userRepository.findByUsername(user);
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }
}
