package com.cloud.collage.collagecloud.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/prueba")
public class PruebaController {

    @GetMapping("/lista")
    public ResponseEntity<?> index(){
        Map<String, Object> res = new HashMap<>();
        res.put("mensage", "hola desde prueba");

        return new ResponseEntity<>(res, HttpStatus.CREATED);
    }
}
