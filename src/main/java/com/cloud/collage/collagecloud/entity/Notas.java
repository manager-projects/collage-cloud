package com.cloud.collage.collagecloud.entity;

public class Notas {
    private Long id;
    private String materias;
    private double notas;

    public Notas() {
    }

    public Notas(String materias, double notas) {
        this.materias = materias;
        this.notas = notas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMaterias() {
        return materias;
    }

    public void setMaterias(String materias) {
        this.materias = materias;
    }

    public double getNotas() {
        return notas;
    }

    public void setNotas(double notas) {
        this.notas = notas;
    }
}
