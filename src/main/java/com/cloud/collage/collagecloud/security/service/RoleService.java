package com.cloud.collage.collagecloud.security.service;

import com.cloud.collage.collagecloud.security.entity.Role;
import com.cloud.collage.collagecloud.security.enums.NameRole;

import java.util.Optional;

public interface RoleService {
    Optional<Role> getNameRole(NameRole role);
    void saveRole(Role role);
}
